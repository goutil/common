/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package stuff

import (
	"gitlab.com/goutil/logger/logger"
	"os"
	"strconv"
	"time"
)

/// Get os.Env or use default values

func EnvOrDefault(name, def string) string {
	if env := os.Getenv(name); env != "" {
		return env
	}
	return def
}

func EnvOrDefaultInt(name string, def int) int {
	if env := os.Getenv(name); env != "" {
		val, err := strconv.ParseInt(env, 10, 64); if err != nil {
			logger.Errorf("Faled to parsing to int env var '%s' -> %s: %v", name, env, err)
		}
		return int(val)
	}
	return def
}

func EnvOrDefaultDuration(name string, def time.Duration) time.Duration {
	if env := os.Getenv(name); env != "" {
		val, err := time.ParseDuration(env); if err != nil {
			logger.Errorf("Faled to parsing to time.Duration env var '%s' -> %s: %v", name, env, err)
		}
		return val
	}
	return def
}

func EnvOrDefaultBool(name string, def bool) bool {
	if env := os.Getenv(name); env != "" {
		val, err := strconv.ParseBool(env); if err != nil {
			logger.Errorf("Faled to parsing to bolean env var '%s' -> %s: %v", name, env, err)
		}
		return val
	}
	return def
}
