/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package stuff

import (
	"fmt"
)

type Any struct {
	val interface{}
}

func (v *Any) String() string {
	if val, ok := v.val.(string); ok {
		return val
	}
	panic(fmt.Sprintf("Can't cast value of type string: %v", v.val))
}

func (v *Any) Int() int64 {
	if val, ok := v.val.(int64); ok {
		return val
	}
	panic(fmt.Sprintf("Can't cast value of type int: %v", v.val))
}

func (v *Any) Float() float64 {
	if val, ok := v.val.(float64); ok {
		return val
	}
	panic(fmt.Sprintf("Can't cast value of type float: %v", v.val))
}
