/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package stuff

func AgrOrDefault(args []interface{}, index int, def interface{}) (val *Any) {
	if len(args) >= index+1 {
		val = &Any{args[index]}
	} else {
		val = &Any{def}
	}
	return
}

